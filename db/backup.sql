--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

-- Started on 2018-05-11 17:17:26

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2833 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16402)
-- Name: aaa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aaa (
    id integer NOT NULL,
    title text
);


ALTER TABLE public.aaa OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16515)
-- Name: kamoku; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kamoku (
    kamoku_c integer NOT NULL,
    kamoku character varying(10) NOT NULL
);


ALTER TABLE public.kamoku OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16525)
-- Name: kekka; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kekka (
    namae_c integer NOT NULL,
    kamoku_c integer NOT NULL,
    nichiji_c integer NOT NULL,
    ten integer NOT NULL
);


ALTER TABLE public.kekka OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16520)
-- Name: nichiji; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nichiji (
    nichiji_c integer NOT NULL,
    nichiji date NOT NULL
);


ALTER TABLE public.nichiji OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24593)
-- Name: n_kekka; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.n_kekka AS
 SELECT kekka.namae_c,
    kekka.kamoku_c,
    kekka.nichiji_c,
    nichiji.nichiji,
    kekka.ten
   FROM (public.kekka
     JOIN public.nichiji ON ((kekka.nichiji_c = nichiji.nichiji_c)));


ALTER TABLE public.n_kekka OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16510)
-- Name: namae; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.namae (
    namae_c integer NOT NULL,
    namae character varying(10) NOT NULL
);


ALTER TABLE public.namae OWNER TO postgres;

--
-- TOC entry 2821 (class 0 OID 16402)
-- Dependencies: 196
-- Data for Name: aaa; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2823 (class 0 OID 16515)
-- Dependencies: 198
-- Data for Name: kamoku; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.kamoku (kamoku_c, kamoku) VALUES (1, '国語');
INSERT INTO public.kamoku (kamoku_c, kamoku) VALUES (2, '算数');
INSERT INTO public.kamoku (kamoku_c, kamoku) VALUES (3, '理科');
INSERT INTO public.kamoku (kamoku_c, kamoku) VALUES (4, '社会');
INSERT INTO public.kamoku (kamoku_c, kamoku) VALUES (5, '英語');


--
-- TOC entry 2825 (class 0 OID 16525)
-- Dependencies: 200
-- Data for Name: kekka; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 1, 9);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 1, 84);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 1, 40);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 1, 52);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 1, 100);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 1, 41);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 1, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 1, 69);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 1, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 2, 64);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 2, 46);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 2, 70);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 2, 24);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 2, 37);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 2, 43);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 2, 20);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 2, 16);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 2, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 3, 39);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 3, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 3, 49);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 3, 7);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 3, 94);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 3, 71);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 3, 35);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 3, 78);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 3, 25);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 4, 72);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 4, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 4, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 4, 83);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 4, 87);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 4, 65);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 4, 64);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 4, 54);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 4, 99);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 5, 70);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 5, 31);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 5, 59);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 5, 33);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 5, 57);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 5, 54);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 5, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 5, 0);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 5, 95);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 6, 43);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 6, 95);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 6, 43);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 6, 68);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 6, 89);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 6, 7);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 6, 10);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 6, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 6, 6);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 7, 3);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 7, 5);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 7, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 7, 77);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 7, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 7, 98);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 7, 14);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 7, 38);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 7, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 8, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 8, 22);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 8, 76);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 8, 66);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 8, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 8, 2);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 8, 6);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 8, 65);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 8, 30);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 9, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 9, 25);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 9, 18);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 9, 4);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 9, 94);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 9, 76);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 9, 68);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 9, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 9, 74);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 10, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 10, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 10, 95);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 10, 30);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 10, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 10, 35);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 10, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 10, 28);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 10, 38);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 11, 28);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 11, 71);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 11, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 11, 22);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 11, 18);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 11, 3);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 11, 84);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 11, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 11, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 12, 35);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 12, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 12, 28);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 12, 69);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 12, 52);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 12, 84);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 12, 57);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 12, 99);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 12, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 13, 5);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 13, 91);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 13, 10);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 13, 47);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 13, 32);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 13, 45);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 13, 7);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 13, 39);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 13, 19);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 14, 30);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 14, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 14, 98);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 14, 91);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 14, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 14, 28);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 14, 61);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 14, 57);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 14, 48);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 15, 26);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 15, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 15, 5);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 15, 95);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 15, 29);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 15, 66);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 15, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 15, 43);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 15, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 16, 44);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 16, 15);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 16, 30);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 16, 85);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 16, 20);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 16, 99);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 16, 53);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 16, 89);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 16, 9);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 17, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 17, 9);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 17, 58);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 17, 63);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 17, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 17, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 17, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 17, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 17, 52);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 18, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 18, 6);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 18, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 18, 87);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 18, 84);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 18, 82);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 18, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 18, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 18, 86);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 19, 57);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 19, 24);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 19, 4);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 19, 27);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 19, 71);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 19, 39);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 19, 87);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 19, 82);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 19, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 20, 39);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 20, 52);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 20, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 20, 46);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 20, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 20, 17);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 20, 13);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 20, 22);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 20, 26);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 21, 70);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 21, 94);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 21, 14);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 21, 27);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 21, 72);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 21, 40);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 21, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 21, 27);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 21, 86);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 22, 94);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 22, 3);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 22, 23);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 22, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 22, 7);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 22, 13);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 22, 22);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 22, 22);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 22, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 23, 84);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 23, 33);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 23, 0);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 23, 58);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 23, 28);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 23, 89);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 23, 40);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 23, 88);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 23, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 24, 23);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 24, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 24, 67);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 24, 14);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 24, 91);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 24, 6);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 24, 72);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 24, 6);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 24, 4);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 25, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 25, 71);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 25, 41);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 25, 83);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 25, 34);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 25, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 25, 53);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 25, 69);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 25, 87);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 26, 92);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 26, 24);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 26, 52);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 26, 59);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 26, 2);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 26, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 26, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 26, 19);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 26, 48);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 27, 18);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 27, 19);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 27, 69);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 27, 9);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 27, 83);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 27, 69);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 27, 37);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 27, 3);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 27, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 28, 23);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 28, 31);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 28, 46);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 28, 97);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 28, 19);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 28, 73);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 28, 29);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 28, 49);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 28, 9);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 29, 8);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 29, 1);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 29, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 29, 78);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 29, 59);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 29, 85);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 29, 33);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 29, 21);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 29, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 30, 98);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 30, 77);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 30, 96);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 30, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 30, 43);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 30, 89);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 30, 65);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 30, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 30, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 31, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 31, 91);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 31, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 31, 77);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 31, 18);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 31, 99);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 31, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 31, 56);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 31, 27);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 32, 10);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 32, 3);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 32, 90);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 32, 78);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 32, 67);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 32, 9);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 32, 41);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 32, 64);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 32, 90);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 33, 41);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 33, 64);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 33, 52);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 33, 98);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 33, 21);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 33, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 33, 23);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 33, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 33, 35);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 34, 8);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 34, 88);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 34, 14);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 34, 46);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 34, 93);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 34, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 34, 34);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 34, 74);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 34, 19);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 35, 33);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 35, 99);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 35, 34);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 35, 94);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 35, 13);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 35, 54);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 35, 25);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 35, 20);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 35, 77);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 1, 36, 95);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 1, 36, 47);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 1, 36, 67);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 1, 36, 78);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 1, 36, 42);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 1, 36, 29);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 1, 36, 32);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 1, 36, 98);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 1, 36, 75);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 2, 37, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 2, 37, 45);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 2, 37, 16);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 2, 37, 38);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 2, 37, 24);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 2, 37, 92);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 2, 37, 44);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 2, 37, 4);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 2, 37, 15);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 3, 38, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 3, 38, 7);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 3, 38, 83);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 3, 38, 60);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 3, 38, 59);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 3, 38, 90);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 3, 38, 20);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 3, 38, 73);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 3, 38, 87);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 4, 39, 17);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 4, 39, 63);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 4, 39, 36);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 4, 39, 63);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 4, 39, 32);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 4, 39, 51);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 4, 39, 87);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 4, 39, 82);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 4, 39, 81);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (1, 5, 40, 80);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (2, 5, 40, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (3, 5, 40, 11);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (4, 5, 40, 89);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (5, 5, 40, 20);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (6, 5, 40, 6);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (7, 5, 40, 12);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (8, 5, 40, 21);
INSERT INTO public.kekka (namae_c, kamoku_c, nichiji_c, ten) VALUES (9, 5, 40, 97);


--
-- TOC entry 2822 (class 0 OID 16510)
-- Dependencies: 197
-- Data for Name: namae; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.namae (namae_c, namae) VALUES (1, '大嶽');
INSERT INTO public.namae (namae_c, namae) VALUES (2, '押木');
INSERT INTO public.namae (namae_c, namae) VALUES (3, '笠見');
INSERT INTO public.namae (namae_c, namae) VALUES (4, '笹川');
INSERT INTO public.namae (namae_c, namae) VALUES (5, '須多');
INSERT INTO public.namae (namae_c, namae) VALUES (6, '髙木');
INSERT INTO public.namae (namae_c, namae) VALUES (7, '殿納');
INSERT INTO public.namae (namae_c, namae) VALUES (8, '松村');
INSERT INTO public.namae (namae_c, namae) VALUES (9, '安田');


--
-- TOC entry 2824 (class 0 OID 16520)
-- Dependencies: 199
-- Data for Name: nichiji; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (1, '2018-04-01');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (2, '2018-04-02');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (3, '2018-04-03');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (4, '2018-04-04');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (5, '2018-04-05');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (6, '2018-04-06');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (7, '2018-04-07');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (8, '2018-04-08');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (9, '2018-04-09');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (10, '2018-04-10');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (11, '2018-04-11');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (12, '2018-04-12');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (13, '2018-04-13');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (14, '2018-04-14');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (15, '2018-04-15');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (16, '2018-04-16');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (17, '2018-04-17');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (18, '2018-04-18');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (19, '2018-04-19');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (20, '2018-04-20');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (21, '2018-04-21');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (22, '2018-04-22');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (23, '2018-04-23');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (24, '2018-04-24');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (25, '2018-04-25');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (26, '2018-04-26');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (27, '2018-04-27');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (28, '2018-04-28');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (29, '2018-04-29');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (30, '2018-04-30');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (31, '2018-05-01');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (32, '2018-05-02');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (33, '2018-05-03');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (34, '2018-05-04');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (35, '2018-05-05');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (36, '2018-05-06');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (37, '2018-05-07');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (38, '2018-05-08');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (39, '2018-05-09');
INSERT INTO public.nichiji (nichiji_c, nichiji) VALUES (40, '2018-05-10');


--
-- TOC entry 2690 (class 2606 OID 16409)
-- Name: aaa aaa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aaa
    ADD CONSTRAINT aaa_pkey PRIMARY KEY (id);


--
-- TOC entry 2694 (class 2606 OID 16519)
-- Name: kamoku kamoku_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kamoku
    ADD CONSTRAINT kamoku_pkey PRIMARY KEY (kamoku_c);


--
-- TOC entry 2698 (class 2606 OID 16529)
-- Name: kekka kekka_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kekka
    ADD CONSTRAINT kekka_pkey PRIMARY KEY (namae_c, kamoku_c, nichiji_c);


--
-- TOC entry 2692 (class 2606 OID 16514)
-- Name: namae namae_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.namae
    ADD CONSTRAINT namae_pkey PRIMARY KEY (namae_c);


--
-- TOC entry 2696 (class 2606 OID 16524)
-- Name: nichiji nichiji_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nichiji
    ADD CONSTRAINT nichiji_pkey PRIMARY KEY (nichiji_c);


-- Completed on 2018-05-11 17:17:26

--
-- PostgreSQL database dump complete
--

