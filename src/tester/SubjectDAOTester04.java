/**
 *
 */
package tester;

import java.util.InputMismatchException;
import java.util.Scanner;

import dao.SubjectDAO;
import dto.SubjectDTO;

/**
 * @author ymats
 *
 */
public class SubjectDAOTester04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//１：SubjectDAOクラスの新しいインスタンスを生成
		SubjectDAO dao = new SubjectDAO();

		try (
				//２a：Scannerクラスの新しいインスタンスを生成
				Scanner scanner = new Scanner(System.in);) {

			//２b：キーボードからint型の値を得る
			int id = scanner.nextInt();

			//３：２を引数にして１のgetメソッドを呼び、戻り値をSubjectDTO型変数subjectに代入する
			SubjectDTO subject = dao.get(id);

			//idフィールドを表示
			System.out.println(subject.getId());

			//titleフィールドを表示
			System.out.println(subject.getTitle());
		} catch (InputMismatchException e) {
			e.printStackTrace();
			System.out.println("キーボードからの入力が変");
		}

	}

}
