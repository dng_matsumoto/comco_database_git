/**
 *
 */
package tester;

import dao.MemberDAO;
import dto.MemberDTO;

/**
 * @author ymats
 *
 */
public class MemberDAOTester02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//１：MemberDAOクラスの新しいインスタンスを生成
		MemberDAO dao = new MemberDAO();

		//２：キーボードからint型の値を得る
		int id = new java.util.Scanner(System.in).nextInt();

		//３：２を引数にして１のgetメソッドを呼び、戻り値をMemberDTO型変数memberに代入する
		MemberDTO member = dao.get(id);

		//idフィールドを表示
		System.out.println(member.getId());

		//nameフィールドを表示
		System.out.println(member.getName());

	}

}
