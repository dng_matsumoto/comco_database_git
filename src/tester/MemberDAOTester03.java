/**
 *
 */
package tester;

import java.util.List;
import java.util.Scanner;

import dao.MemberDAO;
import dto.MemberDTO;

/**
 * @author ymats
 *
 */
public class MemberDAOTester03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//		１：MemberDAOクラスのインスタンスを生成
		MemberDAO dao = new MemberDAO();

		//		２：try-with-resourcesのリソース定義部分でjava.util.ScannerのインスタンスをSystem.inを引数として生成
		try (
				Scanner scanner = new Scanner(System.in);

		) {
			//		３：tryブロック内で以下の処理を行う
			//		　　あ：２で生成したScannerクラスのインスタンスのnextLine()メソッドを呼び出しString型の値を得る
			String name = scanner.nextLine();

			//		　　い：１で生成したMemberDAOクラスのインスタンスのgetByNameメソッドを あ で取得した値を実引数として呼び出し
			//		　　　　返却値をList<MemberDTO>型のmembers変数に代入する
			List<MemberDTO> members = dao.getByName(name);

			//		　　う：いで取得したmembers変数の各要素のidフィールドとnameフィールドを画面に表示する
			for (MemberDTO member : members) {
				System.out.println(member.getId() + "  " + member.getName());

			}
		}

	}

}
