/**
 *
 */
package tester;

import java.util.InputMismatchException;
import java.util.Scanner;

import dao.SubjectDAO;

/**
 * @author ymats
 *
 */
public class SubjectDAOTester05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//１：SubjectDAOクラスの新しいインスタンスを生成
		SubjectDAO dao = new SubjectDAO();

		try (
				//２a：Scannerクラスの新しいインスタンスを生成
				Scanner scannerInt = new Scanner(System.in);

				//２b：Scannerクラスの新しいインスタンスを生成
				Scanner scannerString = new Scanner(System.in);
				) {
			//２ｃ：キーボードからint型の値を得る
			int id = scannerInt.nextInt();
			//２ｄ
			String title = scannerString.nextLine();
			//３：２c,dを引数にして１のupdateTilteメソッドを呼び、戻り値をrowCountに代入する
			int rowCount = dao.updateTitle(id, title);

			//rowCountフィールドを表示
			System.out.println(rowCount);

		} catch (InputMismatchException e) {
			e.printStackTrace();
			System.out.println("キーボードからの入力が変");
		}

	}

}
