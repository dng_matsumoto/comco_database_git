/**
 *
 */
package tester;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import dao.SubjectDAO;
import dto.SubjectDTO;

/**
 * @author ymats
 *
 */
public class SubjectDAOTester02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//１：SubjectDAOクラスの新しいインスタンスを生成
		SubjectDAO dao = new SubjectDAO();
		LocalDate d;
		try (
				//２a：Scannerクラスの新しいインスタンスを生成
				Scanner scanner = new Scanner(System.in);) {

			//２b：キーボードからint型の値を得る
			String title = scanner.nextLine();

			//３：２を引数にして１のgetメソッドを呼び、戻り値をSubjectDTO型変数subjectに代入する
			List<SubjectDTO> subjects = dao.getByTitle(title);

			for (SubjectDTO subject : subjects) {
				//idフィールドを表示
				System.out.println(subject.getId() + " " + subject.getTitle());

			}
		} catch (InputMismatchException e) {
			e.printStackTrace();
			System.out.println("キーボードからの入力が変");
		}

	}

}
