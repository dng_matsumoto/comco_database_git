/**
 *
 */
package tester;

import java.util.List;
import java.util.Scanner;

import dao.ExaminationDAO;
import dto.ExaminationDTO;

/**
 * @author ymats
 *
 */
public class ExaminationDAOTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ExaminationDAO dao = new ExaminationDAO();
		try (
				Scanner scanner = new Scanner(System.in);) {

			{
				System.out.println("getメソッドのテスト");
				System.out.print("id? ");
				int id = scanner.nextInt();

				ExaminationDTO exam = dao.get(id);
				System.out.println(exam.getId() + " " + exam.getMemberId() + " " + exam.getSubjectId() + " "
						+ exam.getExecDate() + " " + exam.getScore());
			}
			{
				System.out.println("-----------------------------------");
				System.out.println("getBySubjectIdメソッドのテスト");
				System.out.print("subjectID? ");
				int subjectId = scanner.nextInt();
				List<ExaminationDTO> exams = dao.getBySubjectId(subjectId);
				for (ExaminationDTO exa : exams) {
					System.out.println(exa.getId() + " " +
							exa.getMemberId() + " " +
							exa.getSubjectId() + " " +
							exa.getExecDate() + " " +
							exa.getScore());
				}
				System.out.println("取得件数 " + exams.size());

			}
			{
				System.out.println("-----------------------------------");
				System.out.println("getByMemberIdメソッドのテスト");
				System.out.print("memberID? ");
				int memberId = scanner.nextInt();
				List<ExaminationDTO> exams = dao.getByMemberId(memberId);
				for (ExaminationDTO exa : exams) {
					System.out.println(exa.getId() + " " +
							exa.getMemberId() + " " +
							exa.getSubjectId() + " " +
							exa.getExecDate() + " " +
							exa.getScore());
				}
				System.out.println("取得件数 " + exams.size());

			}

			{
				System.out.println("-----------------------------------");
				System.out.println("getByMemberIdAndSubjectIdメソッドのテスト");
				System.out.print("memberID? ");
				int memberId = scanner.nextInt();
				System.out.print("subjectID? ");
				int subjectId = scanner.nextInt();
				List<ExaminationDTO> exams = dao.getByMemberIdAndSubjectId(memberId , subjectId);
				for (ExaminationDTO exa : exams) {
					System.out.println(exa.getId() + " " +
							exa.getMemberId() + " " +
							exa.getSubjectId() + " " +
							exa.getExecDate() + " " +
							exa.getScore());
				}
				System.out.println("取得件数 " + exams.size());

			}


		}

	}

}
