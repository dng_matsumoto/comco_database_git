/**
 *
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.MemberDTO;

/**
 * @author ymats
 *
 */
public class MemberDAO {

	public String getName(int id) {
		String sql = "SELECT * FROM members WHERE id = ?;";
		String name = null;//返却値を格納する変数
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			//プレースフォルダに値をセットする
			statement.setInt(1, id);

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				name = result.getString("name");
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}
		return name;
	}

	public void disp(int id) {
		String sql = "SELECT * FROM members WHERE id = ?;";

		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			//プレースフォルダに値をセットする
			statement.setInt(1, id);

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				String mid = result.getString("id");
				String member_name = result.getString("name");
				System.out.printf("id=%3s name=%10s", mid, member_name);
				System.out.println();
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}
	}

	public MemberDTO get(int id) {
		String sql = "SELECT * FROM members WHERE id = ?;";
		MemberDTO member = null;//返却値を格納する変数
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			//プレースフォルダに値をセットする
			statement.setInt(1, id);

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				member = new MemberDTO();
				member.setId(result.getInt("id"));
				member.setName(result.getString("name"));
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}
		return member;
	}

	public List<MemberDTO> getByName(String name) {

		//		１：SQL文作成する
		String sql = "SELECT * FROM members WHERE name LIKE ?;";

		//		２：ArrayList<MemberDTO>型の変数membersを宣言し、
		//		　　新しく引数なしでインスタンス化したArrayList<MemberDTO>型のインスタンスを代入する
		ArrayList<MemberDTO> members = new ArrayList<MemberDTO>();

		//		３：try-with-resourcesのリソース定義部分でConnectionとPreparedStatementを生成する
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			//		４：tryブロックで以下の処理を行う
			//		　　あ：プレースフォルダに値を引数とワイルドカードを組み合わせた文字列をセット
			statement.setString(1, "%" + name + "%");

			//		　　い：PreparedStatementのexecuteQueryメソッドを呼び、返却値をResultSet型のresult変数に取得する
			ResultSet result = statement.executeQuery();

			//		　　う：いで取得したResultSetのnextメソッドの戻り値がtrueである間、以下のａ～ｄの処理を行う
			while (result.next()) {
				//		　　　　ａ：MemberDTO型の変数memberを宣言し新しいMemberDTOのインスタンスを代入する
				MemberDTO member = new MemberDTO();

				//		　　　　ｂ：ａのインスタンスのidフィールドに　い　で取得した行のid列の値をセットする
				member.setId(result.getInt("id"));
//				int id = result.getInt("id");
//				member.setId(id);

				//		　　　　ｃ：ａのインスタンスのnameフィールドに　い　で取得した行のname列の値をセットする
				member.setName(result.getString("name"));

				//		　　　　ｄ：ａのインスタンスを２で生成したインスタンスに加える
				members.add(member);
			}

		}
		//		５：catchブロックで SQLExceptionとNullPointerExceptionを捕獲し、以下の あ の処理を行う
		catch (SQLException | NullPointerException e) {
			//		　　あ：例外インスタンスのprintStackTraceメソッドを呼ぶ
			e.printStackTrace();
		}

		//		６：membersを返却値としてメソッドを終了する
		return members;

	}
}
