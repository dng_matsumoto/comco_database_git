package dao;
/**
 *
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.SubjectDTO;

/**
 * @author ymats
 *
 */
public class SubjectDAO {

	public SubjectDTO get(int id) {
		String sql = "SELECT * FROM subjects WHERE id = ?;";
		SubjectDTO subject = null;//返却値を格納する変数
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			//プレースフォルダに値をセットする
			statement.setInt(1, id);

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				subject = new SubjectDTO();
				subject.setId(result.getInt("id"));
				subject.setTitle(result.getString("title"));
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}
		return subject;

	}

	public List<SubjectDTO> getByTitle(String title) {

		String sql = "SELECT * FROM subjects WHERE title LIKE ?;";

		ArrayList<SubjectDTO> subjects = new ArrayList<SubjectDTO>();

		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {

			statement.setString(1, "%" + title + "%");

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				SubjectDTO subject = new SubjectDTO();

				subject.setId(result.getInt("id"));

				subject.setTitle(result.getString("title"));

				subjects.add(subject);
			}

		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}

		return subjects;

	}

	public int insertSubject(String title) {

		String sql = "INSERT INTO subjects (title) VALUES (?);";
		int result = -1;
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {

			statement.setString(1, title);

			result = statement.executeUpdate();
			return result;

		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return result;
	}

	public int insertSubject(SubjectDTO subject) {
		return insertSubject(subject.getTitle());
	}

	public int updateTitle(int id , String title) {
		String sql = "UPDATE subjects SET title = ? WHERE id = ?;";
		int result = -1;
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {

			statement.setString(1, title);
			statement.setInt(2, id);
			result = statement.executeUpdate();
			return result;

		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return result;

	}
}
