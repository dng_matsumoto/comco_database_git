/**
 *
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.ExaminationDTO;

/**
 * @author ymats
 *
 */
public class ExaminationDAO {

	private static final String url = "jdbc:postgresql://localhost:5432/test01";
	private static final String usr = "postgres";
	private static final String pass = "comco2020";

	/**
	 * examninations表からid列の値が等しい行を得る
	 * @param id 検索条件
	 * @return idが等しい行のExaminationDTO
	 */
	public ExaminationDTO get(int id) {
		ExaminationDTO exam = null;
		String sql = "SELECT * FROM examinations WHERE id = ?;";
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(url, usr, pass);
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			statement.setInt(1, id);
			ResultSet r = statement.executeQuery();

			while (r.next()) {
				exam = setFields(r);
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return exam;
	}

	public List<ExaminationDTO> getBySubjectId(int subjectId) {
		ArrayList<ExaminationDTO> exams = new ArrayList<ExaminationDTO>();
		String sql = "SELECT * FROM examinations WHERE subject_id = ?;";
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(url, usr, pass);
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			statement.setInt(1, subjectId);
			ResultSet r = statement.executeQuery();

			while (r.next()) {
				exams.add(setFields(r));
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}

		return exams;
	}

	public List<ExaminationDTO> getByMemberId(int memberId) {
		ArrayList<ExaminationDTO> exams = new ArrayList<ExaminationDTO>();
		String sql = "SELECT * FROM examinations WHERE member_id = ?;";
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(url, usr, pass);
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			statement.setInt(1, memberId);
			ResultSet r = statement.executeQuery();

			while (r.next()) {
				exams.add(setFields(r));
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}

		return exams;
	}

	public List<ExaminationDTO> getByMemberIdAndSubjectId(int memberId, int subjectId) {
		ArrayList<ExaminationDTO> exams = new ArrayList<ExaminationDTO>();
		String sql = "SELECT * FROM examinations WHERE member_id = ? AND subject_id = ?;";
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(url, usr, pass);
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {
			statement.setInt(1, memberId);
			statement.setInt(2, subjectId);
			ResultSet r = statement.executeQuery();

			while (r.next()) {
				exams.add(setFields(r));
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}

		return exams;
	}

	private ExaminationDTO setFields(ResultSet r) throws SQLException {
		ExaminationDTO exam = new ExaminationDTO();
		exam.setId(r.getInt("id"));
		exam.setMemberId(r.getInt("member_id"));
		exam.setSubjectId(r.getInt("subject_id"));
		exam.setExecDate(r.getDate("exec_date"));
		exam.setScore(r.getInt("score"));
		return exam;
	}
}
