/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * @author ymats
 *
 */
public class MembersTableSelect12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.print("nameの一部? ");
		String input = new Scanner(System.in).nextLine();


		String sql = "SELECT * FROM members WHERE name LIKE '%"
				+ input + "%'"+ ";";
//		System.out.println(sql);
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				Statement statement = connection.createStatement();

				//SQLを実行しResultSet（結果を保持する）を取得
				ResultSet result = statement.executeQuery(sql);

		) {
			while (result.next()) {
				String id = result.getString("id");
				String member_name = result.getString("name");
				System.out.printf("id=%3s name=%10s", id, member_name);
				System.out.println();
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

	}

}
