/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author ymats
 *
 */
public class SubjectTableSelect03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String sql = "SELECT * FROM subjects;";

		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				int id = result.getInt("id");
				int title = result.getInt("title");
				System.out.printf("id=%3d title=%10d", id, title);
				System.out.println();
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

	}

}
