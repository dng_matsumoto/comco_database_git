/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.ZonedDateTime;

/**
 * @author ymats
 *
 */
public class ExaminationsTableSelect07 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ZonedDateTime end = null;
		ZonedDateTime start = null;

		String sql = "SELECT * FROM examinations "
//				+ "WHERE subject_id = 1 AND member_id = 7"
				+ ";";

		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {

			start = ZonedDateTime.now();

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			//合計を保持する変数
			int sum = 0;

			//レコードごとに足し合わせる
			while (result.next()) {
				int score = result.getInt("score");
				sum += score;
			}
			//表示する
			System.out.println("合計=" + sum);
			end = ZonedDateTime.now();

		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

		Duration d = Duration.between(start, end);
		System.out.println(d);
	}

}
