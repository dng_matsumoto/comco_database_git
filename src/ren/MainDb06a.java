/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * resultsテーブルのemployes_id= 3 and subject_id=1のレコードの全フィールドを表示する
 * @author Y.Matsumoto
 *
 */
public class MainDb06a {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		String sql = "SELECT * FROM results WHERE employee_id = 3 AND subject_id =1;";

		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//prepareStatementの生成
				PreparedStatement statement= connection.prepareStatement(sql);

		) {

			//SQLを実行しResultSetを取得
			ResultSet result = statement.executeQuery();
			int total = 0;
			while (result.next()) {
				total += result.getInt("score") ;
			}
			System.out.println(total);
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}
	}

}
