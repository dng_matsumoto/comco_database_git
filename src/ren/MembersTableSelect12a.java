/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author ymats
 *
 */
public class MembersTableSelect12a {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.print("nameの一部? ");
		String input = new Scanner(System.in).nextLine();

		String sql = "SELECT * FROM members WHERE name LIKE ?;";
		//		System.out.println(sql);
		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);

		)

		{
			//プレースフォルダにワイルドカード付きで値をセットする
			statement.setString(1, "%" + input + "%");

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				String id = result.getString("id");
				String member_name = result.getString("name");
				System.out.printf("id=%3s name=%10s", id, member_name);
				System.out.println();
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

	}

}
