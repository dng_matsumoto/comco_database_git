/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * コンソールから入力したemployes_idと subject_idと一致する
 * resultsテーブルの
 * レコードの全フィールドを表示する
 *
 * @author Y.Matsumoto
 *localdate_exec_date
 */
public class MainDb10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		String sql = "SELECT avg(score) , sum(score) , max(score) , min(score) FROM results WHERE employee_id = ? AND subject_id = ? ;";

		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//prepareStatementの生成
				PreparedStatement statement= connection.prepareStatement(sql);

				//スキャナーの生成
				Scanner s = new Scanner(System.in);

		) {

			//文字入力
			System.out.print("employee_id : ");
			int e_id = s.nextInt();
			System.out.print("subject_id : ");
			int s_id = s.nextInt();
			System.out.print("集計方法 1avg 2sum 3max 4min : ");
			int func = s.nextInt();

			statement.setInt(1, e_id);
			statement.setInt(2, s_id);

			//SQLを実行しResultSetを取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
//				String employee_id = result.getString("employee_id");
//				String subject_id = result.getString("subject_id");
//				Date date_exec_date = result.getDate("exec_date");
//				LocalDate localdate_exec_date = date_exec_date.toLocalDate();
				int score = result.getInt(func) ;
				System.out.println(/*employee_id + "\t" + subject_id + "\t" + localdate_exec_date + "\t" + */score);
			}
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}
	}

}
