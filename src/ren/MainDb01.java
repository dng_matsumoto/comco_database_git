/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Y.Matsumoto
 *
 */
public class MainDb01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String sql = "SELECT * FROM members;";

		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);

				//SQLを実行しResultSet（結果を保持する）を取得
				ResultSet result = statement.executeQuery();

		) {
			while (result.next()) {
				String id = result.getString("id");
				String member_name = result.getString("name");
				System.out.printf("id=%3s name=%10s", id, member_name);
				System.out.println();
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}
	}

}
