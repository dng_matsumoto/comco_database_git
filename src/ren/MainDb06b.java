/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * resultsテーブルのemployes_id= 3 and subject_id=1のレコードの全フィールドを表示する
 * @author Y.Matsumoto
 *
 */
public class MainDb06b {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		String sql = "SELECT sum(score) as total FROM results WHERE employee_id = 3 AND subject_id =1 GROUP BY employee_id , subject_id;";

		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//prepareStatementの生成
				PreparedStatement statement= connection.prepareStatement(sql);

		) {

			//SQLを実行しResultSetを取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				int score = result.getInt("total") ;
				System.out.println(score);
			}
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}
	}

}
