/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * resultsテーブルのemployes_id= 3 and subject_id=1のレコードの全フィールドを表示する
 * ただしexec_dateについては日付だけ表示する
 * @author Y.Matsumoto
 *
 */
public class MainDb07 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		String sql = "SELECT * FROM results WHERE employee_id = 3 AND subject_id =1;";

		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//prepareStatementの生成
				PreparedStatement statement= connection.prepareStatement(sql);

		) {

			//SQLを実行しResultSetを取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				String emplyee_id = result.getString("employee_id");
				String subject_id = result.getString("subject_id");
				Date date_exec_date = result.getDate("exec_date");
				LocalDate localdate_exec_date = date_exec_date.toLocalDate();
				int int_exec_date = localdate_exec_date.getDayOfMonth();
				int score = result.getInt("score") ;
				System.out.println(emplyee_id + "\t" + subject_id + "\t" + int_exec_date + "\t" + score);
			}
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}
	}

}
