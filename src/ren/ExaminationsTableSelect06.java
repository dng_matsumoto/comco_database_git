/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author ymats
 *
 */
public class ExaminationsTableSelect06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String sql = "SELECT * FROM examinations "
				+ "WHERE subject_id = 1 AND member_id = 7;";

		try (
				//コネクションの生成（データベースに接続）
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test01", //接続先URI
						"postgres", //ユーザー名
						"comco2020"); //パスワード
				//ステートメントの生成（送信するＳＱＬ・送信した結果を保持するインスタンス）
				PreparedStatement statement = connection.prepareStatement(sql);) {

			//SQLを実行しResultSet（結果を保持する）を取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				int id = result.getInt("id");
				int subjectId = result.getInt("subject_id");
				int memberId = result.getInt("member_id");
				String execDate = result.getString("exec_date");
				int score = result.getInt("score");

				System.out.printf("id=%5d subject_id=%3d member_id=%3d date=%12s score=%3d",
						id, subjectId, memberId, execDate, score);
				System.out.println();
			}
		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

	}

}
