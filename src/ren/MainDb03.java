/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * resultsテーブルの全レコード　全フィールドを表示する
 * @author Y.Matsumoto
 *
 */
public class MainDb03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		String sql = "SELECT * FROM results;";

		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//prepareStatementの生成
				PreparedStatement statement= connection.prepareStatement(sql);

		) {

			//SQLを実行しResultSetを取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				String emplyee_id = result.getString("employee_id");
				String subject_id = result.getString("subject_id");
				String exec_date = result.getString("exec_date");
				int score = result.getInt("score");
				System.out.println(emplyee_id + "\t" + subject_id + "\t" + exec_date + "\t" + score);
			}
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}
	}

}
