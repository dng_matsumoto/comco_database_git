/**
 *
 */
package ren;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author Y.Matsumoto
 *
 */
public class MainDb02 {

	/**
	 * キーボードから２つの数字を入力
	 * 最初に入力した値と、最後に入力した値の間のidをもつ
	 * emplyeesテーブルのレコードの全フィールドを表示
	 * @param args
	 */
	public static void main(String[] args) {

		String sql = "select id,name from employees WHERE id BETWEEN ? AND ? ;";



		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//prepareStatementの生成
				PreparedStatement statement= connection.prepareStatement(sql);

				//スキャナーの生成
				Scanner s = new Scanner(System.in);
		) {

			//文字入力
			System.out.print("最初の数字 : ");
			int begin = s.nextInt();
			System.out.print("最後の数字 : ");
			int end = s.nextInt();


			//prepareStatementに値を設定
			statement.setInt(1, Math.min(begin, end));
			statement.setInt(2, Math.max(begin, end));
			//SQLを実行しResultSetを取得
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				String id = result.getString("id");
				String student_name = result.getString("name");
				System.out.println(id + "\t" + student_name);
			}
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}
	}

}
