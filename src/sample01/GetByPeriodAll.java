/**
 *
 */
package sample01;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author Matsumoto
 *
 */
public class GetByPeriodAll {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner stdIn=new Scanner(System.in);
		System.out.print("from：");
		String from=stdIn.nextLine();
		System.out.print("to：");
		String to=stdIn.nextLine();
		stdIn.close();

		Date d_from = Date.valueOf(from);
		Date d_to = Date.valueOf(to);

		Connection connection = null;
		PreparedStatement statement = null;
		String sql = "select student_name,subject_name,exec_date,score "
				+ "from students join results on students.id=results.student_id "
				+ "join subjects on results.subject_id=subjects.id "
				+ "where exec_date between ? and ?";

		// ドライバファイルの読み込み
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// コネクションの生成
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (connection == null) {
			System.exit(1);
		}

		// ステートメントの生成とデータベースへのアクセス
		try {
			statement = connection.prepareStatement(sql);
			statement.setDate(1, d_from);
			statement.setDate(2, d_to);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				String student_name = result.getString("student_name");
				String subject_name = result.getString("subject_name");
				String exec_date = result.getString("exec_date");
				int score = result.getInt("score");
				System.out.println(student_name + "\t" + subject_name + "\t" + exec_date + "\t" + score);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


}
