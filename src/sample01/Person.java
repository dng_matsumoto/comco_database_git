package sample01;

public class Person {
	private String id_person;
	private String person;
	/**
	 * @return id_person
	 */
	public String getId_person() {
		return id_person;
	}
	/**
	 * @param id_person セットする id_person
	 */
	public void setId_person(String id_person) {
		this.id_person = id_person;
	}
	/**
	 * @return person
	 */
	public String getPerson() {
		return person;
	}
	/**
	 * @param person セットする person
	 */
	public void setPerson(String person) {
		this.person = person;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Student [id_person=");
		builder.append(id_person);
		builder.append(", person=");
		builder.append(person);
		builder.append("]");
		return builder.toString();
	}



}
