/**
 *
 */
package sample01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Matsumoto
 *
 */
public class GetReslutsAll {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		Connection connection = null;
		Statement statement = null;
		String sql = "select * from results;";

		//ドライバファイルの読み込み
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//コネクションの生成
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://192.168.30.105:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if ( connection == null){
			System.exit(1);
		}

		//ステートメントの生成とデータベースへのアクセス
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			while (result.next()) {
				String student_id = result.getString("student_id");
				String subejct_id = result.getString("subject_id");
				String exec_date = result.getString("exec_date");
				int score = result.getInt("score");
				System.out.println(student_id + "\t" + subejct_id + "\t" + exec_date + "\t" + score);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if ( statement != null ){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if ( connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
