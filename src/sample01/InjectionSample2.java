package sample01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class InjectionSample2 {

	public static void main(String[] args) {
		Scanner stdIn=new Scanner(System.in);
		System.out.print("username：");
		String user=stdIn.nextLine();
		System.out.print("password：");
		String password=stdIn.nextLine();
		stdIn.close();

		Connection connection = null;
		Statement statement = null;
		String sql = makesql(user, password);

		// ドライバファイルの読み込み
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// コネクションの生成
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (connection == null) {
			System.exit(1);
		}

		// ステートメントの生成とデータベースへのアクセス
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			if (result.next()) {
				System.out.println("ログイン成功");
			} else {
				System.out.println("ログイン失敗");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String makesql(String user, String password){

		String sql ="SELECT * FROM users WHERE userid = '"+
				user +
				"' AND password = '" + password  + "';";
		return sql;

	}

}
