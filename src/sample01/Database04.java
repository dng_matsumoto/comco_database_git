/**
 *
 */
package sample01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Matsumoto
 *
 */
public class Database04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		Connection connection = null;
		PreparedStatement pstatement = null;
		String sql = "select id,student_name,address,tel from students "+
				" where id = ?";
		ArrayList<Person> students = new ArrayList<Person>();

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if ( connection == null){
			System.exit(1);
		}

		try {
			pstatement = connection.prepareStatement(sql);
			pstatement.setString(1, args[0]);
			ResultSet result = pstatement.executeQuery();
			while (result.next()) {
				Person s = new Person();
//				s.setId(result.getString("id"));
//				s.setStudent_name(result.getString("student_name"));
//				s.setAddress(result.getString("address"));
//				s.setTel(result.getString("tel"));
				students.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if ( pstatement != null ){
				try {
					pstatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if ( connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		for ( Person student : students){
			System.out.println(student);
		}
	}

}
