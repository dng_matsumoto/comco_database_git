package sample01;

public class InjectionSample {

	public static void main(String[] args) {

		//シングルクォーテーション
		String user = "test";
		String password = "' OR 'A' = 'A";

		System.out.println(makesql(user , password));


		//セミコロンで別の文を挿入
		user ="";
		password = "'; DELETE FROM users WHERE 'A' = 'A";

		System.out.println(makesql(user , password));

		//コメントで後ろを無効化
		user ="'; DELETE FROM users WHERE 'A' = 'A' --";
		password ="";

		System.out.println(makesql(user , password));
	}

	public static String makesql(String user, String password){

		String sql ="SELECT * FROM users WHERE userid = '"+
				user +
				"' AND password = '" + password  + "';";
		return sql;

	}

}
