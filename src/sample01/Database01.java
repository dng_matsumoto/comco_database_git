/**
 *
 */
package sample01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Matsumoto
 *
 */
public class Database01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		Connection connection = null;
		Statement statement = null;
		String sql = "select id_person,person from t_person order by id_person;";

		//ドライバファイルの読み込み
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//コネクションの生成
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if ( connection == null){
			System.exit(1);
		}

		//ステートメントの生成とデータベースへのアクセス
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			while (result.next()) {
				int id = result.getInt("id_person");
				String person = result.getString("person");
				System.out.println(id + "\t" + person );
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if ( statement != null ){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if ( connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
