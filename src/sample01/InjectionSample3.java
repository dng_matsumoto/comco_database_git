package sample01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class InjectionSample3 {

	public static void main(String[] args) {
		Scanner stdIn=new Scanner(System.in);
		System.out.print("username：");
		String user=stdIn.nextLine();
		System.out.print("password：");
		String password=stdIn.nextLine();
		stdIn.close();

		Connection connection = null;
		PreparedStatement pst = null;
		String sql = makesql(user, password);

		// ドライバファイルの読み込み
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// コネクションの生成
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (connection == null) {
			System.exit(1);
		}

		// ステートメントの生成とデータベースへのアクセス
		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, user);
			pst.setString(2, password);
			ResultSet result = pst.executeQuery();
			if (result.next()) {
				System.out.println("ログイン成功");
			} else {
				System.out.println("ログイン失敗");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String makesql(String user, String password){

		String sql ="SELECT * FROM users WHERE userid = ? AND password = ?;";
		return sql;

	}

}
