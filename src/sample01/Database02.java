/**
 *
 */
package sample01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author Matsumoto
 *
 */
public class Database02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		Connection connection = null;
		Statement statement = null;
		String sql = "select id_person,person from t_person";

		//取得したstudentデータを複数まとめて格納するArrayList
		ArrayList<Person> students = new ArrayList<Person>();

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/test",
					"postgres",
					"comco");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if ( connection == null){
			System.exit(1);
		}

		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			while (result.next()) {
				//レコードがあったら新しいstudentインスタンスを生成
				Person s = new Person();

				//Setterを用いて値を設定
				s.setId_person(result.getString("id_person"));
				s.setPerson(result.getString("person"));

				//ArrayListに追加
				students.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if ( statement != null ){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if ( connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		for ( Person student : students){
			System.out.println(student);
		}
	}

}
