/**
 *
 */
package sample02;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * try-with-resource版
 * @author Matsumoto
 *
 */
public class Database01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String sql = "select id,name from employees order by id;";

		try (
				//コネクションの生成
				Connection connection = DriverManager.getConnection(
						"jdbc:postgresql://localhost:5432/test",
						"postgres",
						"comco2019");
				//ステートメントの生成
				Statement statement = connection.createStatement();

				//SQLを実行しResultSetを取得
				ResultSet result = statement.executeQuery(sql);

		) {
			while (result.next()) {
				String id = result.getString("id");
				String student_name = result.getString("name");
				System.out.println(id + "\t" + student_name);
			}
		} catch (NullPointerException | SQLException  e) {
			e.printStackTrace();
		}

	}

}
